package com.tcs.test.microservice.repository;

import com.tcs.test.microservice.entity.Employee;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class EmployeeRepo {

    public Map<Integer, Employee> data = new HashMap<>();

    @PostConstruct
    public void init() {
        Employee e = new Employee();
        e.setId(1);
        e.setName("Amar Ingole");
        e.setEmail("amar.ingole@gmail.com");
        data.put(1, e);
    }

    public Employee findById(int id) {

        if(!data.containsKey(id)) {
            throw new RuntimeException("data not found");
        }
        return data.get(id);
    }
}
