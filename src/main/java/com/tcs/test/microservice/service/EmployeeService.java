package com.tcs.test.microservice.service;

import com.tcs.test.microservice.entity.Employee;
import com.tcs.test.microservice.mapper.EmployeeMapper;
import com.tcs.test.microservice.model.EmployeeResponse;
import com.tcs.test.microservice.repository.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
 
    @Autowired
    private EmployeeRepo employeeRepo;
 
    @Autowired
    private EmployeeMapper mapper;
 
    public EmployeeResponse getEmployeeById(int id) {
        Employee employee = employeeRepo.findById(id);
        EmployeeResponse employeeResponse = mapper.map(employee);
        return employeeResponse;
    }
 
}