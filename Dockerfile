FROM openjdk:8-jdk-alpine
MAINTAINER baeldung.com
COPY target/MicroServiceTest-0.0.1-SNAPSHOT.jar MicroServiceTest-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/MicroServiceTest-0.0.1-SNAPSHOT.jar"]